import os


def install_runner():
    '''
    首次安装runner时使用此方法
    :return:
    '''
    if os.path.isfile('/etc/gitlab-runner/config.toml'):
        is_file = os.system('rm -rf /etc/gitlab-runner/config.toml')
        assert is_file == 0, "config.yaml del failed"

    job_tag = input("job_tag:dev,master:")
    token = input("token:")

    s1 = os.system("sudo docker run --rm -t -i -v /etc/gitlab-runner:/etc/gitlab-runner gitlab/gitlab-runner register \
                   -n -u https://gitlab.com/ -r {token} --executor docker --docker-image docker \
                   --tag-list {job_tag}".format(token=token, job_tag=job_tag))

    assert s1 == 0, "runner failed"

    os.system("sudo chmod 777 /etc/gitlab-runner/config.toml")
    with open('/etc/gitlab-runner/config.toml', 'r') as f:
        s = f.read()
        a = s.replace('volumes = ["/cache"]',
                      'volumes = ["/var/run/docker.sock:/var/run/docker.sock","/cache"]')

    with open('/etc/gitlab-runner/config.toml', 'w') as f:
        f.write(a)
    s2 = os.system("sudo docker run -d --name gitlab-runner --restart always \
                   -v /etc/gitlab-runner:/etc/gitlab-runner \
                   -v /var/run/docker.sock:/var/run/docker.sock \
                   gitlab/gitlab-runner:latest")
    assert s2 == 0, "runner failed"

    s3 = os.system("sudo docker restart gitlab-runner")
    assert s3 == 0, "runner ok"
    print("runner ok")


if __name__ == "__main__":
    install_runner()

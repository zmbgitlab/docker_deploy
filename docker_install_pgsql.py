import os


def docker_install_pgsql():
    s1 = os.system("docker pull postgres:9.6")
    assert s1 == 0, "pgsql failed"
    s2 = os.system("docker run --name docker_pgsql -e POSTGRES_PASSWORD=zmb123456 -p 5432:5432 -d postgres:9.6")
    assert s2 == 0, "pgsql run failed"
    print('run pgsql ok')


if __name__ == "__main__":
    docker_install_pgsql()

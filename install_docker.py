import os


def install_docker():

    step0 = os.system("apt-get update")
    assert step0 == 0, "apt update ok"

    step1 = os.system("apt install docker.io -y")
    assert step1 == 0, "docker failed"

    os.system("sudo mkdir -p /etc/docker")
    step3 = os.system("""
        sudo tee /etc/docker/daemon.json <<-'EOF'
    {
    "registry-mirrors": ["https://m6wlkecl.mirror.aliyuncs.com"]
    }
    EOF
        """)
    assert step3 == 0, "docker registry failed"
    print('docker install ok, registry daemon aliyuncs ok')

    os.system("service docker restart")
    print('docker restart ok')

    aaa = os.system("docker pull docker:latest")
    assert aaa == 0, "docker:latest"

    bbb = os.system("docker pull python:3.5")
    assert bbb == 0, "python:3.5"
    print('docker install ok')


if __name__ == "__main__":
    install_docker()

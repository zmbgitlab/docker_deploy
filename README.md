###以下为flask项目在gitlab上CI/CD、build、deploy简洁步骤,[宿主机服务器系统为 Ubuntu 16.04]###

#首先注册个dockerhub账号,注册地址:https://hub.docker.com/;然后创建个镜像仓库。

#[安装docker步骤及常用命令]:
    1. 安装docker命令: apt install docker.io -y
    2. 查看docker版本命令: docker -v
     
#[docker常用命令]:
    查看状态：service docker status
    查看版本：docker -v
    启动：service docker start
    重启：service docker restart
    构建镜像：docker build -t hs_auth:20181221 .
    运行容器：docker run -d -p 5000:5000 hsauth:0.1, 若镜像为latest版本则tag可以省略,否则必须指定版本号
    查看镜像列表：docker images ls
    查看正在运行的容器：docker ps -a
    修改镜像仓库名与标签名：docker tag <image id> xxx:xxx
    强制删除镜像：docker rmi -f <image id>
    强制删除容器：docker rm -f <container id>
    进入镜像容器内: docker exec -ti <container id> bash
    查看镜像的某个容器状态: docker ps | grep <container id>
         
#[docker安装、注册GitLab-Runner]:
    python脚本安装: python3 install_docker.py即可安装、注册、运行runner

#[docker安装pgsql、sqlserver、mysql的步骤]:
    #docker安装、运行pgsql:
        1. docker pull postgres:9.6
        2. docker run --name docker_pgsql -e POSTGRES_PASSWORD=zmb123456 -p 5432:5432 -d postgres:9.6
        
        PS: 
            run: 创建并运行一个容器;
            --name: 指定创建容器名称;
            -e POSTGRES_PASSWORD=admin123456: 设置环境变量,指定数据库的登录口令为admin123456;
            -p 5432:5432: 端口映射将容器的5432端口映射到外部机器的5432端口;
            -d postgres:9.4: 指定使用postgres:9.4作为镜像;
        
        ##postgres镜像默认的用户名为postgres,登陆口令为创建容器是指定的值
        
    #docker安装、运行sqlserver: 未通过
        1. 安装：docker pull microsoft/mssql-server-linux
        2. 运行并挂载本地目录“/root/mssql_data”到docker容器中"/opt/mssql_data"：
        docker run -d -e 'ACCEPT_EULA=Y' -e 'SA_PASSWORD=admin123456' -p 9510:1433 --name aliyn_mssql -v /root/mssql_data:/opt/mssql_data mcr.microsoft.com/mssql/server:2017-latest

        
    #docker安装、运行mysql:
        1. docker pull mysql:5.6
        2. docker run -p 3306:3306 --name aliyun_mysql -v $PWD/conf:/etc/mysql/conf.d -v $PWD/logs:/logs -v $PWD/data:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=admin123456 -d mysql:5.6
        
        PS: 
            -p 3306:3306: 将容器的 3306 端口映射到宿主机的 3306 端口;
            -v $PWD/conf:/etc/mysql/conf.d: 将宿主机当前目录下的 conf/my.cnf 挂载到容器的 /etc/mysql/my.cnf;
            -v $PWD/logs:/logs: 将宿主机当前目录下的 logs 目录挂载到容器的 /logs;
            -v $PWD/data:/var/lib/mysql: 将宿主机当前目录下的 data 目录挂载到容器的 /var/lib/mysql;
            -e MYSQL_ROOT_PASSWORD=admin123456: 初始化 root 用户的密码;
        
        3. 修改mysql的root账号允许任何客户端连接:
            3.1 进入mysql容器bash: docker exec -ti <container id> bash
            3.2 登录mysql: mysql -u root -p
            3.3 修改root 可以通过任何客户端连接: GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' IDENTIFIED BY 'admin123456' WITH GRANT OPTION;
            3.4 flush privileges;